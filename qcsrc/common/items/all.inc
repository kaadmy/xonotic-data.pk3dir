/** If you register a new item, make sure to add it to this list */
#include "item/ammo.qc"
#include "item/armor.qc"
#include "item/health.qc"
#include "item/jetpack.qc"
#include "item/pickup.qc"
#include "item/powerup.qc"
